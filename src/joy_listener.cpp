#include "ros/ros.h"
#include "std_msgs/Float64.h"


void chatterCallback(const std_msgs::Float64::ConstPtr& msg)
{
  ROS_INFO("I heard: [%f]", msg->data);
}

int main(int argc, char **argv)
{

  ros::init(argc, argv, "joy_listener");
  ros::NodeHandle n;
  ros::Subscriber sub = n.subscribe("joy_XY", 1000, chatterCallback);
  ros::spin();

  return 0;
}
